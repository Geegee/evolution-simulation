;; parameters of the simulation
(defparameter *width* 100)
(defparameter *height* 30)
(defparameter *jungle* '(45 10 10 10))
(defparameter *plant-energy* 80)
(defparameter *reproduction-energy* 200)
(defparameter *num-animals-record* (list 1))
(defparameter *update-period* 10)
(defparameter *day* 0)
(defparameter *age-of-deceased-record* nil)
(defparameter *ids* (list 0))
(defparameter *birthdays* (list 0))
(defstruct animal x y energy dir genes age birth id)
(defparameter *animals*
  (list (make-animal :x (ash *width* -1)
                     :y (ash *height* -1)
                     :energy 1000
                     :dir 0
                     :genes (loop repeat 8
                                  collecting (1+ (random 10)))
                     :age 0
                     :id 0
                     :birth 0)))

(defparameter *plants* (make-hash-table :test #'equal))
