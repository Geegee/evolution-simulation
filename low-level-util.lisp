;; utility functions

(defun reconstr-lis-without-elements (lis el-ind &optional (outputlis nil) (counter 0))
  ;; enter a list and 
  ;; an ordered list that mark the elements that should be removed. No element should lie outside interval [0, ..., n-1]
  ;; with n the length of the list lis. Reverses the order of the input list. 
  (let ((head (car lis)) (tail (cdr lis)) (counter-new (1+ counter)) (ind-head (car el-ind)) (ind-tail (cdr el-ind)))
    ;; either we completely walked through the list or the list of indices
    (cond (ind-tail (if (/= ind-head counter)
                         (reconstr-lis-without-elements tail el-ind (cons head outputlis) counter-new)
                         (reconstr-lis-without-elements tail ind-tail outputlis counter-new)
                         ))
          (tail (if (eq ind-head counter)
                     (append tail outputlis)
                     (reconstr-lis-without-elements tail el-ind (cons head outputlis) counter-new)
                     ))
          (t (if (/= ind-head counter)
                 (cons head outputlis)
                 outputlis))
    )
    )
  )

(defun reverse-li (li &optional (outputli nil))
  (let ((head (car li)) (tail (cdr li)))
    (if tail
        (reverse-li tail (cons head outputli))
        (cons head outputli)))
  )

(defun remove-nth (lis nth)
  (append (subseq lis 0 nth) (subseq lis (1+ nth)))
  )

(defun index-a-lis (lis &optional (outputlis nil) (counter 0))
  (let ((head (car lis)) (tail (cdr lis)))
    (if tail
        (index-a-lis tail (push (cons counter head) outputlis) (1+ counter))
        (push (cons counter head) outputlis)))
  )

(defun create-indice-list (len &optional (outputlis nil))
  (if (/= len 0)
      (create-indice-list (1- len) (cons (1- len) outputlis))
      outputlis)
  )

(defun sum (li &key add)
  (if li
      (let ((new-add (+ add (car li))) (tail (cdr li)))
        (sum tail :add new-add))
      add))

(defun el-to-lis (el)
  (cons el nil))

(defun split-li (li &key help even-lis uneven-lis)
  ;; split list into even and uneven indices
  (if li
      (if (evenp help)
          (split-li (cdr li) :help (1+ help) :even-lis (append even-lis (el-to-lis (car li))) :uneven-lis uneven-lis)
          (split-li (cdr li) :help (1+ help) :uneven-lis (append uneven-lis (el-to-lis (car li))) :even-lis even-lis)
          )
      (cons even-lis uneven-lis)
      )
  )

(defun insert-els (lis &rest rest)
  ;; take a list and insert the rest into it
  (loop for el in rest
        do (push el lis))
  lis
  )
