(load "parameters.lisp")
(load "low-level-util.lisp")
(load "velocity-extraction.lisp")
;; plant related functions
(defun random-plant (left top width height)
  (let ((pos (cons (+ left (random width)) (+ top (random height)))))
    (setf (gethash pos *plants*) t))
  )

(defun add-plants ()
  (apply #'random-plant *jungle*)
  (random-plant 0 0 *width* *height*))

;; animal actions
(defun move (animal)
  (let ((dir (animal-dir animal))
        (x (animal-x animal))
        (y (animal-y animal)))
    (setf (animal-x animal) (mod (+ x
                                    (cond ((and (>= dir 2) (< dir 5)) 1)
                                          ((or (= dir 1) (= dir 5)) 0)
                                          (t -1))
                                    *width*)
                                 *width*))
    (setf (animal-y animal) (mod (+ y (cond ((and (>= dir 0) (< dir 3)) -1)
                                           ((and (>= dir 4) (< dir 7)) 1)
                                           (t 0))
                                   *height*)
                                *height*))
    (decf (animal-energy animal))))

(defun turn (animal)
  (let ((x (random (apply #'+ (animal-genes animal)))))
    (labels ((angle (genes x)
               (let ((xnu (- x (car genes))))
                 (if (< xnu 0)
                     0
                     (1+ (angle (cdr genes) xnu))))))
    (setf (animal-dir animal)
          (mod (+ (animal-dir animal) (angle (animal-genes animal) x))
               8)))))

(defun eat (animal)
  (let ((pos (cons (animal-x animal) (animal-y animal))))
    (when (gethash pos *plants*)
      (incf (animal-energy animal) *plant-energy*)
      (remhash pos *plants*))))

(defun reproduce (animal)
  (let ((e (animal-energy animal)))
    (when (>= e *reproduction-energy*)
      (setf (animal-energy animal) (ash e -1))
      (let ((animal-nu (copy-structure animal))
            (genes (copy-list (animal-genes animal)))
            (mutation (random 8))
            (max-id (get-max-id *animals*)))
        (setf (nth mutation genes) (max 1 (+ (nth mutation genes) (random 3) -1)))
        (setf (animal-genes animal-nu) genes)
        ;; give the new animal a unique id
        (setf (animal-id animal-nu) (1+ max-id))
        (setf (animal-birth animal-nu) *day*)
        (setf (animal-age animal-nu) 0)
        (push (1+ max-id) *ids*)
        (push *day* *birthdays*)
        (push animal-nu *animals*)))))

;; functions operating on animals
(defun update-age (animal)
  (incf (animal-age animal))
  )

(defun update-animal-number ()
  (eq (mod *day* *update-period*) 0))

(defun deadp (animal)
  (<= (animal-energy animal) 0))

(defun alivep (animal)
  (> (animal-energy animal) 0))

(defun get-max-id (animals)
  (let ((ids (mapcar #'animal-id animals)))
    (apply #'max ids)))
 
(defun consume-animals (animals deceased-ledger &optional (living-animals nil))
  ;; broken function to recursively gather statistics on the animals.
  (let ((animal (car animals)) (rem-animals (cdr animals)))
    (if rem-animals
        (progn (if (alivep animal)
                   (push animal living-animals)
                   (setf deceased-ledger (cons (list (animal-age animal) (animal-birth animal) (animal-id animal) *day*) deceased-ledger))
                   )
               (consume-animals rem-animals deceased-ledger living-animals))
        (if (alivep animal)
            (push animal living-animals)
            (setf deceased-ledger (cons (list (animal-age animal) (animal-birth animal) (animal-id animal) *day*) deceased-ledger))
            ))
    (cons living-animals deceased-ledger)))

(defun gather-statistics (animals statistics)
  ;; non recursive function to gather statistics on the animals 
  (let ((alive-animals nil))
    (loop for animal in animals
          do (if (alivep animal)
                 (push animal alive-animals)
                 (push (animal-statistics animal) statistics)))
    (cons alive-animals statistics))
  )

(defun animal-statistics (animal)
  ;; statistics of an individual animal 
  (list (animal-birth animal) (animal-age animal) (animal-avg-velocity animal) (animal-id animal) *day*))

(defun update-world ()
  ;; this structure of updating a variable is something I see quiet often and seems clumsy
  ;; is there a better way to do this?
  (let ((result (gather-statistics *animals* *age-of-deceased-record*)))
    (setf *age-of-deceased-record* (cdr result))
    (setf *animals* (car result)))

  (if (update-animal-number)
      (setq *num-animals-record* (cons (length *animals*) *num-animals-record*)))
  (mapc (lambda (animal)
          (turn animal)
          (move animal)
          (eat animal)
          (reproduce animal)
          (update-age animal))
        *animals*)
  (add-plants)
  (incf *day*))

(defun draw-world ()
  (loop for y
          below *height*
        do (progn (fresh-line)
                  (princ "|")
                  (loop for x
                          below *width*
                        do (princ (cond ((some (lambda (animal)
                                                 (and (= (animal-x animal) x)
                                                         (= (animal-y animal) y)))
                                                            *animals*)
                                         #\M)
                                         ((gethash (cons x y) *plants*) #\*)
                                         (t #\space))))
                  (princ "|"))))

(defun evolution ()
  (draw-world)
  (fresh-line)
  (let ((str (read-line)))
    (cond ((equal str "quit") ())
          (t (let ((x (parse-integer str :junk-allowed t)))
               (if x
                   (loop for i
                           below x
                         do (update-world)
                         if (zerop (mod i 1000))
                           do (princ #\.))
                   (update-world))
               (evolution))))))
