(load "parameters.lisp")
(load "low-level-util.lisp")
;; functions to extract the velocities
(defun normalize-genes (genes)
  (let ((val (sum genes :add 0)))
    (labels ((divbyval (input)
               (/ input val)))
    (mapcar #'divbyval genes))
    )
  )


;; now we check how far we move on average in x / y per turn
;; The genes with even index are the ones that move along the corners.
;; The genes with uneven index are the ones that move diagonal
;; First deal with the movement of

(defun straight-avg-mov (vals)
  ;; again split into even and uneven (even is north south, uneven is east west)
  (let ((sp-li (split-li vals :help 0)))
    (let ((even-vals (car sp-li)) (uneven-vals (cdr sp-li)))
      (cons (- (car even-vals) (cadr even-vals)) (- (car uneven-vals) (cadr uneven-vals)))
      )
    )
)


(defun diagonal-avg-mov (vals)
  ;; split the input list into something that can be handled by straight-avg-mov
  (let ((diag-avg (straight-avg-mov vals)))
    (let ((north-east (car diag-avg)) (south-east (cdr diag-avg)))
      (cons (- (/ north-east 2) (/ south-east 2)) (- (/ north-east 2) (/ south-east 2)))
      )
    )
  )

;; finally calculate the average movement by combining everything
(defun average-movement (genes)
  (let ((ev-uev-genes (split-li genes :help 0)))
    (let ((ev-genes (car ev-uev-genes)) (uev-genes (cdr ev-uev-genes)))
      (let ((xy-avg-ns (straight-avg-mov uev-genes)) (xy-avg-diag (diagonal-avg-mov ev-genes)))
        (add-vec xy-avg-ns (mult-tuple 2 xy-avg-diag))
    )
    )
    )
  )

(defun add-vec (vec0 vec1)
  ;; add two two dimensional vectors
  (let ((x0 (car vec0)) (x1 (car vec1)) (y0 (cdr vec0)) (y1 (cdr vec1)))
    (cons (+ x0 x1) (+ y0 y1))
    )
  )

(defun mult-vec (sc vec)
  ;; multiply a vector by a scaling factor
  (labels ((mult-by (val)
             (* sc val)))
  (mapcar #'mult-by vec))
  )
(defun mult-tuple (sc tup)
  (let ((fst (car tup)) (snd (cdr tup)))
    (cons (* sc fst) (* sc snd))))

(defun vec-length (vec)
  (sqrt (+ (square (car vec)) (square (cdr vec))))
  )

(defun square (num)
  (* num num))
;; now I can calculate the average movement
;; next step is to loop through the list of animals and make a histogram
;; write the average movement to a list

(defun animal-avg-velocity (animal)
  (let ((genes (animal-genes animal)))
    (vec-length (average-movement (normalize-genes genes))))
  )

(defun extract-avg-movements (animals)
  (let ((all-genes (mapcar #'normalize-genes (mapcar #'animal-genes animals))) (avg-movements nil))
    (setq avg-movements (mapcar #'average-movement all-genes)))
  )

(defun extract-avg-velocities (avg-movements)
  (mapcar #'vec-length avg-movements))


