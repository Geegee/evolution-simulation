;; write results to file
;; write age-ofdeceased-record to files
;; TODO create a function that can be run to save the results
(load "low-level-util.lisp")

(defparameter *filename* "age-of-deceased-record3.txt")
(defparameter *header* "# birth age-of-death avg-vel id day~%")

(with-open-file (str *filename*
                     :direction :output
                     :if-exists :supersede
                     :if-does-not-exist :create)
  (format str *header*)
  (loop for records in *age-of-deceased-record*
        do (let ((args (insert-els records (create-control-str (length records)) str)))
             (apply #'format args))
        )
  )

;; utilities for creating the format string
(defun create-control-str (len &optional (cstr ""))
  (if (eq len 0)
      (concatenate 'string cstr "~%")
      (create-control-str (1- len) (concatenate 'string "~d " cstr))
      )
)
